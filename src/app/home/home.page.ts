import { Component } from "@angular/core";
import { HCE } from "@ionic-native/hce/ngx";
import { Ndef, NFC } from "@ionic-native/nfc/ngx";
import { ToastController } from "@ionic/angular";
import jseu from "js-encoding-utils";

@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"],
})
export class HomePage {
  constructor(
    private nfc: NFC,
    private ndef: Ndef,
    private hce: HCE,
    private toastCtrl: ToastController
  ) {}

  async scanNFC() {
    await this.nfc
      .enabled()
      .then(() => this.addListenNFC())
      .catch((err) => {
        this.presentToast(err);
      });
  }

  async addListenNFC() {
    this.nfc
      .transceive("00A40400080000000000000001")
      .then((response) => {
        return this.presentToast(jseu.encoder.arrayBufferToHexString(response));
      })
      .catch((err) => this.presentToast(err));
  }

  async presentToast(message) {
    const toast = await this.toastCtrl.create({
      message,
      duration: 3000,
    });
    toast.present();
  }
}
